#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266WebServer.h>

#define CLIENT_ID "labsudportal"
#define USER "******"
#define PASS "******"

char* ssid     = "****";
char* password = "*****";

const char* mqtt_server ="192.168.111.250";

WiFiClient espClient;
PubSubClient client(espClient);

const char* update_path = "/firmware";
const char* update_username = "******";
const char* update_password = "*******";
ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

IPAddress ip(192, 168, 111, 223);
IPAddress gateway(192, 168, 111, 254);
IPAddress subnet(255, 255, 255, 0);

#define OTA_HOSTNAME "esp8266_portail"
#define OTA_PASS "****"
#define OTA_PORT 1234

#define RELAY D1

void setup() 
{

  Serial.begin(115200);

  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, LOW);

  initWifi();
  initMdns();
  initMQTT();
  initOta();
  initHTTPOTA();

}

void loop() 
{

  manageMQTT();
  manageOTA();

}



void openPortal()
{

  client.publish("labsud/portail/encours", "1");
  digitalWrite(RELAY, HIGH);
  delay(500);
  digitalWrite(RELAY, LOW);
  client.publish("labsud/portail/encours", "0");

}

void initWifi() {

  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.config(ip, gateway, subnet);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(1000);

}


byte initMQTT()
{
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  reconnect();
}

void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  openPortal();

}

void manageMQTT()
{

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

void reconnect() {

  while (!client.connected()) {

    Serial.print("Attempting MQTT connection...");
#ifdef USER
    if (client.connect(CLIENT_ID, USER, PASS)) {
#else
    if (client.connect(CLIENT_ID)) {
#endif
      Serial.println("connected");
      client.subscribe("labsud/portail/ouvre");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void initOta()
{
  ArduinoOTA.setPort(OTA_PORT);
  ArduinoOTA.setHostname(OTA_HOSTNAME);

  // No authentication by default
  ArduinoOTA.setPassword(OTA_PASS);


  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();

}

void initMdns()
{

  if (!MDNS.begin(OTA_HOSTNAME)) {
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  MDNS.addService("http", "tcp", 80);
  Serial.println("mDNS responder started");

}

void initHTTPOTA()
{
  httpUpdater.setup(&httpServer, update_path, update_username, update_password);
  httpServer.begin();
}

void manageOTA()
{
  httpServer.handleClient();
  ArduinoOTA.handle();
}
