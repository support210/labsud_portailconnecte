#include <PubSubClient.h>
#include <ESP8266WiFi.h>

//Wifi
char* ssid     = "****";
char* password = "*****";

//MQTT
const char* mqtt_server ="192.168.111.250";
#define CLIENT_ID "labsudportal"
#define USER "******"
#define PASS "******"
#define TOPIC_OPEN "labsud/portail/ouvre"

WiFiClient espClient;
PubSubClient client(espClient);

// Relais
#define RELAY D1

void setup() 
{

  Serial.begin(115200);

  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, LOW);

  initWifi();
  initMQTT();

}

void loop() 
{

  manageMQTT();
  manageOTA();

}


void openPortal()
{

  digitalWrite(RELAY, HIGH);
  delay(500);
  digitalWrite(RELAY, LOW);

}

void initWifi() {

  delay(10);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  delay(1000);

}


byte initMQTT()
{

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  reconnect();

}

void callback(char* topic, byte* payload, unsigned int length) 
{

  openPortal();

}

void manageMQTT()
{

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

void reconnect() {

  while (!client.connected()) {

    
#ifdef USER
    if (client.connect(CLIENT_ID, USER, PASS)) {
#else
    if (client.connect(CLIENT_ID)) {
#endif
      client.subscribe(TOPIC_OPEN);
    } else {
      delay(5000);
    }
  }
}
}

